const std = @import("std");
// copied from zig reference
const maxInt = std.math.maxInt;

pub fn parseInt(buf: []const u8, radix: u8) !u32 {
    var x: u32 = 0;

    for (buf) |c| {
        if (c == 0) {
            return x;
        }
        const digit = charToDigit(c);

        if (digit >= radix) {
            return error.InvalidChar;
        }

        // x *= radix
        if (@mulWithOverflow(u32, x, radix, &x)) {
            return error.Overflow;
        }

        // x += digit
        if (@addWithOverflow(u32, x, digit, &x)) {
            return error.Overflow;
        }
    }

    return x;
}

fn charToDigit(c: u8) u8 {
    return switch (c) {
        '0'...'9' => c - '0',
        'A'...'Z' => c - 'A' + 10,
        'a'...'z' => c - 'a' + 10,
        else => maxInt(u8),
    };
}
// end copy
