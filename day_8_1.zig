// https://adventofcode.com/2022/day/8
const std = @import("std");
const print = std.debug.print;

fn xytolinear(x: u32, y: u32) u32 {
    return x * 99 + y;
}

pub fn main() !void {
    var file = try std.fs.cwd().openFile("data/day_8.txt", .{});
    defer file.close();
    var buf_reader = std.io.bufferedReader(file.reader());
    var reader = buf_reader.reader();
    var buffer: [100]u8 = undefined;
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    var side_length: u32 = 10;

    var trees = std.ArrayList(u8).init(allocator);
    defer trees.deinit();

    var xr: u32 = 0;
    while (try reader.readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        if (side_length < line.len) {
            side_length = @intCast(u32, line.len);
            try trees.resize(line.len * line.len);
        }
        for (line) |char, idx| {
            trees.items[xytolinear(xr, @intCast(u32, idx))] = char - '0';
        }
        xr += 1;
    }
    var visible: [99 * 99]bool = undefined;

    for (visible) |_, idx| {
        visible[idx] = false;
    }

    var vis = std.AutoHashMap(u32, u8).init(allocator);
    defer vis.deinit();

    var x: u32 = 0;
    var y: u32 = 0;

    while (x < 99) : (x += 1) {
        y = 0;
        var lowest: i32 = -1;
        while (y < 99) : (y += 1) {
            const idx = xytolinear(x, y);
            print("x{d}y{d}idx{d} ", .{ x, y, idx });
            var height: i32 = trees.items[idx];
            if (height > lowest) {
                visible[idx] = true;
                lowest = height;
            }
            // if (height == 9) {
            // break;
            //}
        }
        y = 98;
        lowest = -1;
        while (y < 99) : (y -%= 1) {
            const idx = xytolinear(x, y);
            var height: i32 = trees.items[idx];
            if (height > lowest) {
                visible[idx] = true;
                lowest = height;
            }
           // if (height == 9) {
            //    break;
            //}
        }
    }
    y = 0;
    while (y < 99) : (y += 1) {
        x = 0;
        var lowest: i32 = -1;
        while (x < 99) : (x += 1) {
            const idx = xytolinear(x, y);
            var height: i32 = trees.items[idx];
            if (height > lowest) {
                visible[idx] = true;
                lowest = height;
            }
            //if (height == 9) {
            //    break;
            //}
        }
        x = 98;
        lowest = -1;
        while (x < 99) : (x -%= 1) {
            const idx = xytolinear(x, y);
            var height: i32 = trees.items[idx];
            if (height > lowest) {
                visible[idx] = true;
                lowest = height;
            }
            //if (height == 9) {
             //   break;
            //}
        }
    }

    print("{d}, {d}", .{ trees.items[0], trees.items.len });
    const edgetrees = side_length * 4 - 4;
    print("{d} items in hashmap, {d} on the edge, {d} total\n", .{ vis.count(), edgetrees, vis.count() + edgetrees });
    print("{d}, {d}, {d}\n", .{ visible[0], visible[1], visible[99] });
    var ntrees: u32 = 0;
    for (visible) |v| {
        if (v) {
            ntrees += 1;
        }
    }

    var printArray: [99*99]u8 = undefined;

    for (visible) |b, idx| {
      if (b) {
        printArray[idx] = 1;
      } else {
        printArray[idx] = 0;
      }
    }

    x = 0;
    y = 0;
    while (x < 99) : (x+=1) {
      y = 0;
      while (y < 99) : (y+=1) {
        print("{d}", .{printArray[xytolinear(x, y)]});
      }
      print("\n", .{});
    }

    print("{d} via bool array\n", .{ntrees});
    return;
}
