// https://adventofcode.com/2022/day/4
const std = @import("std");
const intparse = @import("intparse.zig");
const String = @import("zig-string.zig").String;

pub fn main() anyerror!void {
    //const stdout = std.io.getStdOut();
    const arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const print = std.debug.print;
    var file = try std.fs.cwd().openFile("data/day_4.txt", .{});
    defer file.close();
    var buf_reader = std.io.bufferedReader(file.reader());
    var reader = buf_reader.reader();
    var buffer: [100]u8 = undefined;
    // var line = reader.readUntilDelimiterOrEof(&buffer, '\n');
    // _ = try stdout.writer().print("{s}\n", .{line});
    var bounds = [4]u32{ 0, 0, 0, 187 };
    var contained: u32 = 0;
    while (try reader.readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        var i: u32 = 0;
        var buf: [20]u8 = undefined;
        var char_count: u32 = 0;
        for (line) |c| {
            //print("{c}, i:{d}, cc:{d}\n", .{c, i, char_count});
            if (c == '-') {
                buf[char_count] = 0;
                bounds[i] = intparse.parseInt(&buf, 10) catch 0;
                //for (buf) |tc| {
                //  try print("{c}", .{tc});
                //}
                char_count = 0;
                //try print("\n{d}\n", .{bounds[i]});
                i += 1;
            } else if (c == ',') {
                buf[char_count] = 0;
                bounds[i] = intparse.parseInt(&buf, 10) catch 0;
                char_count = 0;
                i += 1;
            } else {
                buf[char_count] = c;
                char_count += 1;
            }
        }
        buf[char_count] = 0;
        bounds[i] = intparse.parseInt(&buf, 10) catch 0;
        //print("{c}, {c}, {c}, {c}\n", .{line[0], line[2], line[4], line[6]});
        //print("{d}, {d}, {d}, {d}\n", .{bounds[0], bounds[1], bounds[2], bounds[3]});
        //try print("{d}, {d}\n", .{bounds[0], bounds[1]});
        if ((bounds[0] <= bounds[3] and bounds[0] >= bounds[2]) or
            (bounds[1] >= bounds[2] and bounds[1] <= bounds[3]) or
            (bounds[2] >= bounds[0] and bounds[2] <= bounds[1]) or
            (bounds[3] >= bounds[0] and bounds[3] <= bounds[1]))
        {
            contained += 1;
        }
        //break;
    }
    print("{d} intervals where one is superfluous\n", .{contained});
    return;
}
