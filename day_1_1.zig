const std = @import("std");

// copied from zig reference
const maxInt = std.math.maxInt;

pub fn parseInt(buf: []const u8, radix: u8) !u32 {
    var x: u32 = 0;

    for (buf) |c| {
        const digit = charToDigit(c);

        if (digit >= radix) {
            return error.InvalidChar;
        }

        // x *= radix
        if (@mulWithOverflow(u32, x, radix, &x)) {
            return error.Overflow;
        }

        // x += digit
        if (@addWithOverflow(u32, x, digit, &x)) {
            return error.Overflow;
        }
    }

    return x;
}

fn charToDigit(c: u8) u8 {
    return switch (c) {
        '0' ... '9' => c - '0',
        'A' ... 'Z' => c - 'A' + 10,
        'a' ... 'z' => c - 'a' + 10,
        else => maxInt(u8),
    };
}
// end copy



pub fn main() !void {
    const stdout = std.io.getStdOut();
    var file = try std.fs.cwd().openFile("./day1_1.in", .{});
    defer file.close();
    var buf_reader = std.io.bufferedReader(file.reader());
    var reader = buf_reader.reader();
    
    var highestCalories:u32 = 0;
    var calorieCounter:u32 = 0;

    var buffer: [100]u8 = undefined;
    // var line = reader.readUntilDelimiterOrEof(&buffer, '\n');
    // _ = try stdout.writer().print("{s}\n", .{line});
    while (try reader.readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        const linelen = line.len;
        if (linelen == 0) {
            highestCalories = std.math.max(calorieCounter, highestCalories);
            calorieCounter = 0;
        } else {
          const calories = parseInt(line, 10) catch 0;
          calorieCounter += calories;
        }
    }
    try stdout.writer().print("The elf with the highest calories has {d}", .{highestCalories});
    return;
}
