// https://adventofcode.com/2022/day/8
const std = @import("std");
const print = std.debug.print;

fn xytolinear(x: u32, y: u32) u32 {
    return x * 99 + y;
}

fn mutateindex(idx: u32, diff: i8) u32 {
    if (diff < 0) {
        return idx -% 1;
    } else {
        return idx +% 1;
    }
}

pub fn main() !void {
    var file = try std.fs.cwd().openFile("data/day_8.txt", .{});
    defer file.close();
    var buf_reader = std.io.bufferedReader(file.reader());
    var reader = buf_reader.reader();
    var buffer: [100]u8 = undefined;
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    var side_length: u32 = 10;

    var trees = std.ArrayList(u8).init(allocator);
    defer trees.deinit();

    var xr: u32 = 0;
    while (try reader.readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        if (side_length < line.len) {
            side_length = @intCast(u32, line.len);
            try trees.resize(line.len * line.len);
        }
        for (line) |char, idx| {
            trees.items[xytolinear(xr, @intCast(u32, idx))] = char - '0';
        }
        xr += 1;
    }
    var visible: [99 * 99]bool = undefined;

    for (visible) |_, idx| {
        visible[idx] = false;
    }

    var vis = std.AutoHashMap(u32, u8).init(allocator);
    defer vis.deinit();

    var x: u32 = 0;
    var y: u32 = 0;
    var highestscore: u32 = 0;
    var besttree: u32 = 0;

    const directions = [4]i8{ -1, 1, -1, 1 };
    const indices = [4]u8{ 0, 0, 1, 1 };

    while (x < 99) : (x += 1) {
        y = 0;
        while (y < 99) : (y += 1) {
            const idx = xytolinear(x, y);
            var height: i32 = trees.items[idx];
            var score: u32 = 1;
            for (directions) |dir, diridx| {
                var curpos = [2]u32{ x, y };
                var dirscore: u32 = 0;
                curpos[indices[diridx]] = mutateindex(curpos[indices[diridx]], dir);
                while (x < 99) : (curpos[indices[diridx]] = mutateindex(curpos[indices[diridx]], dir)) {
                    var locx = curpos[0];
                    var locy = curpos[1];
                    if (locx >= 99 or locy >= 99) {
                        break;
                    }
                    print("{d}\n", .{curpos});
                    const localidx = xytolinear(locx, locy);
                    var locheight = trees.items[localidx];
                    dirscore += 1;
                    switch (locx) {
                        0, 98 => break,
                        else => {},
                    }
                    switch (locy) {
                        0, 98 => break,
                        else => {},
                    }
                    if (locheight >= height) {
                        break;
                    }
                }
                score *= dirscore;
                if (score == 0) {
                    break;
                }
            }
            if (score > highestscore) {
                highestscore = score;
                besttree = idx;
            }
        }
    }
    print("highest scenic score is {d}", .{highestscore});
    return;
}
