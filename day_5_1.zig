// https://adventofcode.com/2022/day/5
const std = @import("std");

pub fn main() !void {
    //const stdout = std.io.getStdOut();
    var file = try std.fs.cwd().openFile("data/day_5.txt", .{});
    defer file.close();

    const print = std.debug.print;

    var buf_reader = std.io.bufferedReader(file.reader());
    var reader = buf_reader.reader();
    var buffer: [100]u8 = undefined;
    // var line = reader.readUntilDelimiterOrEof(&buffer, '\n');
    // _ = try stdout.writer().print("{s}\n", .{line});
    const nperstack = 50;
    var stacks: [9 * nperstack]u8 = undefined;
    for (stacks) |_, idx| {
        stacks[idx] = 0;
    }
    var stackindices: [9]u16 = undefined;
    for (stackindices) |_, idx| {
        stackindices[idx] = @intCast(u16, 8 + idx * nperstack);
    }
    var nreads: [9]u8 = undefined;
    for (nreads) |_, idx| {
        nreads[idx] = 0;
    }
    var nlines: u16 = 0;
    while (try reader.readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        if (line.len == 0) {
            continue;
        }
        if (line[line.len - 1] == ']') {
            var i: u32 = 0;
            i = 1;
            print("{d}st char: {c}, idx: {d}\n", .{ i, line[i], stackindices[i] });
            var idx: u32 = 0;
            while (i < line.len) {
                stackindices[idx] -= 1;
                if (line[i] <= 'Z' and line[i] >= 'A') {
                    stacks[stackindices[idx]] = line[i];
                    nreads[idx] += 1;
                }

                idx += 1;
                i += 4;
            }
            i = 0;
            while (i < 9) : (i += 1) {
                var j: u32 = 0;
                while (j < 25) : (j += 1) {
                    print("{c}", .{stacks[i * nperstack + j]});
                }
                print("\n", .{});
            }
            print("\n", .{});
        } else if (line[0] == ' ') {
            if (line[1] == '1') {
                for (nreads) |_, idx| {
                    stackindices[idx] += nreads[idx];
                }
            }
            continue;
        } else {
            print("now breaking after reading {d} lines\n", .{nlines});
            var i: u32 = 0;

            var count: u8 = 0;
            var offset: u8 = 0;
            if (line[6] == ' ') {
                count = line[5] - '0';
            } else {
                count = 10 * (line[5] - '0') + (line[6] - '0');
                offset = 1;
            }
            var start = line[12 + offset] - '1';
            var target = line[17 + offset] - '1';

            //print("{d}, {d}, {d}\n", .{ count, start, target });
            i = 0;
            while (i < count) : (i += 1) {
                //print("i: {d}, char: {c}, idx: {d}\n", .{ i, stacks[stackindices[start] - 1], stackindices[start] });
                stacks[stackindices[target]] = stacks[stackindices[start] - 1];
                stacks[stackindices[start] - 1] = 0;
                stackindices[target] += 1;
                stackindices[start] -= 1;
            }
        }
        nlines += 1;
    }

    print("{c}, {d}\n", .{ stacks[stackindices[0] - 1], stackindices[0] });
    var i: u16 = 0;

    while (i < 9) : (i += 1) {
        var j: u32 = 0;
        while (j < 25) : (j += 1) {
            print("{c}", .{stacks[i * nperstack + j]});
        }
        print("\n", .{});
    }
    print("\n", .{});
    i = 0;
    while (i < 9) : (i += 1) {
        print("{d} ", .{stackindices[i]});
    }
    print("\n", .{});
    i = 0;
    while (i < 9) : (i += 1) {
        print("{c}", .{stacks[stackindices[i] - 1]});
    }
    print("\n", .{});
    return;
}
