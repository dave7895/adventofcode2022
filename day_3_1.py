#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 12:49:13 2022

@author: d.weingut
"""

import numpy as np

f = open("day_3.txt")

counter = 0

with open("day_3.txt", 'r') as f:
    for line in f:
        length = len(line)
        middle = int(length/2)
        s1 = line[:middle]
        s2 = line[middle:]
        intsct = np.intersect1d([c for c in s1], [c for c in s2])
        print(intsct)
        if len(intsct) != 0:
            doublechar = ord(intsct[0])
            value = doublechar - ord('a') + 1 if (doublechar >= ord('a')) else doublechar - ord('A') + 27
            counter += value
            
print(counter)