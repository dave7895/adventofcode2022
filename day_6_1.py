#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 12:28:27 2022

@author: d.weingut
"""
import numpy as np
import collections
d = collections.deque(["w", "w", "w", "z"], maxlen=4)

f = open("day_6.txt", "r")
line = f.readline()

nchars = 0

for c in line:
    d.append(c)
    nchars += 1
    if len(np.unique(d)) == 4:
        print(nchars)
        break